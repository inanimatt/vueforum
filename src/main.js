import Vue from 'vue'
import App from './App'

Vue.use(require('vue-resource'))

Vue.http.interceptors.push({

  request: function (request) {
    console.log(request)

    // Don't leak the auth token outside the app
    if (!request.root) {
      return request
    }

    // NOTE: normally you'd get the JWT from the API like this maybe

    // var jwt = window.sessionStorage.getItem('jwt')
    //
    // if (!jwt) {
    //   console.log('Fetching JWT')
    //
    //   Vue.http.get('/sso/token').then(function (authResponse) {
    //     jwt = authResponse.data.token
    //     window.sessionStorage.setItem('jwt', jwt)
    //
    //     request.headers.Authorization = 'Bearer ' + jwt
    //     return request
    //   }, function (authResponse) {
    //     console.log('Error retrieving JWT')
    //   })
    // } else {
    //   request.headers.Authorization = 'Bearer ' + jwt
    // }

    // … but let's hard code it for today
    request.headers.Authorization = 'Bearer eyJ0eXAiOiJKV1MiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VybmFtZSI6InRlc3RqaXZlIiwiaWF0IjoxNDUyNzgzNzU4LCJleHAiOjE0NTI4NzAxNTh9.upWtm7n6xnnaIQeDMvFUzCNkzmJO6sjv68oah1moPBw'

    return request
  },

  response: function (response) {
    if (response.status === 403) {
      alert('JWT expired')
    }

    return response
  }

})

/* eslint-disable no-new */
new Vue({
  el: 'body',
  components: { App },
  data: {
    topics: []
  }
})

